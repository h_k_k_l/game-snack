void CreateMap()
{
	SetColor(10);
	//Tường trên.
	int x = 10, y = 1;
	while (x < 100)
	{
		gotoXY(x, y);
		cout << "+";
		x++;
		Sleep(5);
	}
	//Tường phải.
	x = 100; y = 1;
	while (y < 26)
	{
		gotoXY(x, y);
		cout << "+";
		y++;
		Sleep(9);
	}
	//Tường dưới.
	x = 100; y = 26;
	while (x >=10)
	{
		gotoXY(x, y);
		cout << "+";
		x--;
		Sleep(5);
	}
	//Tường trái.
	x = 10; y = 26;
	while (y >= 1)
	{
		gotoXY(x, y);
		cout << "+";
		y--;
		Sleep(9);
	}
	Sleep(200);
}

void DrawSnake(Snake s)
{
	for (int i = 0; i < s.Length; i++)
	{
		gotoXY(s.x[i], s.y[i]);
		if(i==0) // Đầu rắn.
			cout << "0";
		else
			cout << "o";
	}
}

void InitSnake(Snake &s)
{
	//Rắn ban đầu nằm ở giữa map và hường từu trái sang phải.
	int x = 50, y = 13;
	s.Length = 4;
	s.Score = 0;
	for (int i = 0; i < s.Length; i++)
	{
		s.x[i] = x;
		s.y[i] = y;
		x--;
	}
}
