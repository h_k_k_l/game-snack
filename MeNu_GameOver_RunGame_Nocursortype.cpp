int Menu()
{
	int choice = 0, i;
	srand(time(NULL));
	while (choice==0)
	{
		if (_kbhit())
			choice = _getch();
		system("cls");
		i = rand() % 15 + 1;
		SetColor(i);
		cout << "   ===     =      =      =      ==    ==  ========" << endl;
		cout << " ==   ==   ==     =     = =     ==   =    ==" << endl;
		cout << "==         =  =   =    == ==    ==  =     ==" << endl;
		cout << "   ===     =   =  =   ==   ==   ====      ======" << endl;
		cout << "       ==  =    = =  === = ===  ==  =     ==" << endl;
		cout << " ==   ==   =     ==  =       =  ==   =    ==" << endl;
		cout << "   ===     =      =  ==     ==  ==    ==  ========" << endl;
		cout << endl;
		cout << "     PRESS ANY KEY TO CONTINUE :V" << endl;
		cout << endl;
		Sleep(300);
	}
	cout << "1_Classic" << endl;
	cout << "2_Obstacle" << endl;
	cout << "TYPE YOUR CHOICE: ";
	cin >> choice;
	return choice;
}

void GameOver(Snake s)
{
	gotoXY(10, 30);
	cout << "  =====        =      =      =    =====     =        =  ========  =======" << endl;
	gotoXY(10, 31);
	cout << "==     ==     = =     ==    ==  ==     ==   =        =  ==        ==     =" << endl;
	gotoXY(10, 32);
	cout << "==     ==    == ==    = =  = =  ==     ==   ==      ==  ==        ==     =" << endl;
	gotoXY(10, 33);
	cout << "==          ==   ==   =  ==  =  ==     ==    ==    ==   ======    == ====       SCORE: "<<s.Score << endl;
	gotoXY(10, 34);
	cout << "==   ====  === = ===  =      =  ==     ==     ==  ==    ==        == ==" << endl;
	gotoXY(10, 35);
	cout << "==     ==  =       =  =      =  ==     ==      ====     ==        ==  ==" << endl;
	gotoXY(10, 36);
	cout << "  =====    ==     ==  =      =    =====         ==      ========  ==   ==" << endl;
	gotoXY(10, 37);
}

void RunGame()
{
	system("cls");
	bool Over = false;
	Snake s;
	int x = 40, y = 8;
	int xq = 0, yq = 0;
	int check = 2;
	int Time = 5;
	CreateMap();
	InitSnake(s);
	DrawSnake(s);
	SetColor(10);
	while(Time > -1)
	{
		gotoXY(40, 20);
		cout << "Start in: " << Time << " seconds";
		Sleep(1000);
		gotoXY(40, 20);
		cout << "                   ";
		Time--;
	}
	srand(time(NULL));
	CreateFood(s, xq, yq);
	while (!Over)
	{
		DelDrawSnake(s);
		if (_kbhit())
		{
			char kitu = _getch();
			if (kitu == -32)
			{
				kitu = _getch();
				if (kitu == 72 && check != 0)
				{
					check = 1;
				}
				else if (kitu == 80 && check != 1)
				{
					check = 0;
				}
				else if (kitu == 77 && check != 3)
				{
					check = 2;
				}
				else if (kitu == 75 && check != 2)
				{
					check = 3;
				}
			}
		}
		if (check == 0)
		{
			y++;
		}
		else if (check == 1)
		{
			y--;
		}
		else if (check == 2)
		{
			x++;
		}
		else if (check == 3)
		{
			x--;
		}
		SnakeWorks(s, x, y, xq, yq);
		Over = BiteTail(s);
		if (Over)
			continue;
		Over = CrashWall(s.x[0], s.y[0]);
		Sleep(110);
	}
	GameOver(s);
}

void Nocursortype()
{
	CONSOLE_CURSOR_INFO Info;
	Info.bVisible = FALSE;
	Info.dwSize = 20;
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &Info);
}