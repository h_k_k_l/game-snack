void CreateFood(Snake s, int& x, int& y)
{
	do
	{
		x = rand() % 89 + 11;
		y = rand() % 24 + 2;
	} while (FoodOverideSnake(s, x, y));
	int i = rand() % 15 + 1;
	SetColor(i);
	gotoXY(x, y);
	cout << "$";
}

bool FoodOverideSnake(Snake s, int x, int y)
{
	//Kiểm tra food được tạo ra trùng với tọa độ của rắn.
	for (int i = 0; i < s.Length; i++)
	{
		if (x == s.x[i] && y == s.y[i])
			return true;
	}
	return false;
}

bool EatFood(int x0, int y0, int xqua, int yqua)
{
	//Kiểm tra đầu rắn trùng với tọa độ food.
	if (x0 == xqua && y0 == yqua)
		return true;
	return false;
}