void SnakeWorks(Snake& s, int x, int y, int& xqua, int& yqua)
{
	Add(s, s.x, x);
	Add(s, s.y, y);
	if (!EatFood(s.x[0], s.y[0], xqua, yqua))
	{
		Del(s, s.x, s.Length - 1);
		Del(s, s.y, s.Length - 1);
	}
	else
	{
		s.Length--;
		CreateFood(s, xqua, yqua);
		s.Score = s.Score + 10;
	}
	DrawSnake(s);
}

bool CrashWall(int x, int y)
{
	//Kiểm tra đầu rắn trùng với tọa của tường.
	if (y == 1 && (x >= 10 && x <= 100))
		return true;
	if (y == 26 && (x >= 10 && x <= 100))
		return true;
	if (x == 100 && (y >= 1 && y <= 26))
		return true;
	if (x == 10 && (y >= 1 && y <= 26))
		return true;
	return false;
}

bool BiteTail(Snake s)
{
	//Kiểm tra đầu rắn chạm trừng với tọa độ của rắn.
	for (int i = 1; i < s.Length; i++)
	{
		if ((s.x[0] == s.x[i]) && (s.y[0] == s.y[i]))
			return true;
	}
	return false;
}