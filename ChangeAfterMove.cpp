void Add(Snake& s, int a[], int x)
{
	for (int i = s.Length; i > 0; i--)
	{
		a[i] = a[i - 1];
	}
	a[0] = x;
	s.Length++;
}

void Del(Snake& s, int a[], int vt)
{
	for (int i = vt; i < s.Length; i++)
	{
		a[i] = a[i + 1];
	}
	s.Length--;
}

void DelDrawSnake(Snake& s)
{
	for (int i = 0; i < s.Length; i++)
	{
		gotoXY(s.x[i], s.y[i]);
		cout << " "; //In ra khoảng trắng để xóa đi rắn trước khi in ra một rắn mới.
	}
}