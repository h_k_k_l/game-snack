void CreateMap2()
{
	SetColor(10);
	//Tường trên.
	int x = 10, y = 1;
	while (x < 100)
	{
		gotoXY(x, y);
		cout << "+";
		x++;
		Sleep(5);
	}
	//Tường phải.
	x = 100; y = 1;
	while (y < 26)
	{
		gotoXY(x, y);
		cout << "+";
		y++;
		Sleep(9);
	}
	//Tường dưới.
	x = 100; y = 26;
	while (x >= 10)
	{
		gotoXY(x, y);
		cout << "+";
		x--;
		Sleep(5);
	}
	//Tường trái.
	x = 10; y = 26;
	while (y >= 1)
	{
		gotoXY(x, y);
		cout << "+";
		y--;
		Sleep(9);
	}
	x = 11; y = 9;
	while (x<=40)
	{
		gotoXY(x, y);
		cout << "+";
		x++;
	}
	x = 65; y = 2;
	while (y<=13)
	{
		gotoXY(x, y);
		cout << "+";
		y++;
	}
	x = 28; y = 20;
	while (x<=75)
	{
		gotoXY(x, y);
		cout << "+";
		x++;
	}
}

void CreateFood2(Snake s, int &x, int &y)
{
	do
	{
		x = rand() % 89 + 11;
		y = rand() % 24 + 2;
	} while (FoodOverideSnake(s, x, y) || (y == 9 && x >= 11 && x <= 40) || (x == 65 && y >= 2 && y <= 13) || (y == 20 && x >= 28 && x <= 75));
	int i = rand() % 15 + 1;
	SetColor(i);
	gotoXY(x, y);
	cout << "$";
}

bool CrashWall2(int x, int y)
{
	//Kiểm tra đầu rắn trùng với tọa của tường.
	if (y == 1 && (x >= 10 && x <= 100))
		return true;
	if (y == 26 && (x >= 10 && x <= 100))
		return true;
	if (x == 100 && (y >= 1 && y <= 26))
		return true;
	if (x == 10 && (y >= 1 && y <= 26))
		return true;
	if ((y == 9 && x >= 11 && x <= 40) || (x == 65 && y >= 2 && y <= 13) || (y == 20 && x >= 28 && x <= 75))
		return true;
	return false;
}